package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

/******************************************************************************/
// App struct
type App struct {
	Router *mux.Router
	DB     *sql.DB
}

/******************************************************************************/
// App general methods
// App initialization method
func (a *App) Initialize(user, password, dbname string) {
	connectionString :=
		fmt.Sprintf("user=%s password=%s dbname=%s", user, password, dbname)
	var err error
	a.DB, err = sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
	}

	a.Router = mux.NewRouter()
}

// Register Routes!
func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/bill", a.getAllBills).Methods("GET")
	a.Router.HandleFunc("/bill", a.createBill).Methods("POST")
	a.Router.HandleFunc("/bill/{id:[0-9]+}", a.getBill).Methods("GET")
	a.Router.HandleFunc("/bill/{id:[0-9]+}", a.updateBill).Methods("PUT")
	a.Router.HandleFunc("/bill/{id:[0-9]+}", a.deleteBill).Methods("DELETE")

	a.Router.HandleFunc("/legislator", a.getAllLegislators).Methods("GET")
	a.Router.HandleFunc("/legislator", a.createLegislator).Methods("POST")
	a.Router.HandleFunc("/legislator/{id:[0-9]+}", a.getLegislator).Methods("GET")
	a.Router.HandleFunc("/legislator/{id:[0-9]+}", a.updateLegislator).Methods("PUT")
	a.Router.HandleFunc("/legislator/{id:[0-9]+}", a.deleteLegislator).Methods("DELETE")
}

// App run method
func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(":8000", a.Router))
}

/******************************************************************************/
// legislator routes
// App get specific legislator method
func (a *App) getLegislator(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Legislator ID")
		return
	}

	l := legislator{ID: id}
	if err := l.getLegislator(a.DB); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Legislator not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	respondWithJSON(w, http.StatusOK, l)
}

// Get all Legislators Method
func (a *App) getAllLegislators(w http.ResponseWriter, r *http.Request) {
	legislators, err := getAllLegislators(a.DB)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, legislators)
}

// Create Legislator Method
func (a *App) createLegislator(w http.ResponseWriter, r *http.Request) {
	var l legislator
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&l); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer r.Body.Close()

	if err := l.createLegislator(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusCreated, l)
}

// Update Legislator Method
func (a *App) updateLegislator(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid legislator ID")
		return
	}

	var l legislator
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&l); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid resquest payload")
		return
	}
	defer r.Body.Close()
	l.ID = id

	if err := l.updateLegislator(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, l)
}

// Delete Legislator method
func (a *App) deleteLegislator(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid bill ID")
		return
	}

	l := legislator{ID: id}
	if err := l.deleteLegislator(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}

/******************************************************************************/
// bill routes
// App get specific bill method
func (a *App) getBill(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Bill ID")
		return
	}

	b := bill{ID: id}
	if err := b.getBill(a.DB); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Bill not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	respondWithJSON(w, http.StatusOK, b)
}

// Get all Bills Method
func (a *App) getAllBills(w http.ResponseWriter, r *http.Request) {
	bills, err := getAllBills(a.DB)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, bills)
}

// Create Bill Method
func (a *App) createBill(w http.ResponseWriter, r *http.Request) {
	var b bill
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&b); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer r.Body.Close()

	if err := b.createBill(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusCreated, b)
}

// Update Bill Method
func (a *App) updateBill(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid bill ID")
		return
	}

	var b bill
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&b); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid resquest payload")
		return
	}
	defer r.Body.Close()
	b.ID = id

	if err := b.updateBill(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, b)
}

// Delete Bill method
func (a *App) deleteBill(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid bill ID")
		return
	}

	b := bill{ID: id}
	if err := b.deleteBill(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}

/******************************************************************************/
// General Utility Functions
// Error Response Function
func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

// JSON Response Function
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
