package main

import (
	"database/sql"
	"time"
)

/******************************************************************************/
type legislator struct {
	ID        int    `json:"id"`
	LastName  string `json:"lastname"`
	FirstName string `json:"firstname"`
	StartYear int    `json:"startyear"`
	EndYear   int    `json:"endyear"`
	Standing  bool   `json:"standing"`
	Party     string `json:"party"`
}

type bill struct {
	ID               int       `json:"id"`
	Title            string    `json:"id"`
	Session          int       `json:"session"`
	Pass             bool      `json:"pass"`
	VoteDate         time.Time `json:"votedate"`
	VoteYay          int       `json:"voteyay"`
	VoteYayNames     []string  `json:"voteyaynames"`
	VoteNay          int       `json:"votenay"`
	VoteNayNames     []string  `json:"votenaynames"`
	VoteAbstain      int       `json:"voteabstain"`
	VoteAbstainNames []string  `json:"voteabstainnames"`
	VoteAbsent       int       `json:"voteabsent"`
	VoteAbsentNames  []string  `json:"voteabsentnames"`
}

/******************************************************************************/
// Legislator Methods
func (l *legislator) getLegislator(db *sql.DB) error {
	return db.QueryRow("SELECT name FROM legislators WHERE id=$1",
		l.ID).Scan(&l.LastName)
}

func (l *legislator) updateLegislator(db *sql.DB) error {
	_, err :=
		db.Exec(`UPDATE legislators SET lastname=$1, firstname=$2, startyear=$3`+
			`endyear=$4, standing=$5, party=$6 WHERE id=$7`, l.LastName, l.FirstName,
			l.StartYear, l.EndYear, l.Standing, l.Party, l.ID)

	return err
}

func (l *legislator) deleteLegislator(db *sql.DB) error {
	_, err := db.Exec("DELETE FROM legislators WHERE id=$1", l.ID)

	return err
}

func (l *legislator) createLegislator(db *sql.DB) error {
	err := db.QueryRow(
		`INSERT INTO legislators(lastname, firstname, startyear, endyear, standing,`+
			`party VALUES($1, $2, $3, $4, $5, $6) RETURNING id`, l.FirstName, l.LastName,
		l.StartYear, l.EndYear, l.Standing, l.Party).Scan(&l.ID)

	if err != nil {
		return err
	}

	return nil
}

/******************************************************************************/
// bill Methods
func (b *bill) getBill(db *sql.DB) error {
	return db.QueryRow("SELECT name FROM bills WHERE id=$1",
		b.ID).Scan(&b.Title)
}

func (b *bill) updateBill(db *sql.DB) error {
	_, err := db.Exec(
		`UPDATE legislators SET title=$1, session=$2, pass=$3, votedate=$4, `+
			`voteyay=$5, voteyaynames=$6, votenay=$7, votenaynames=$8, voteabstain=$9, `+
			`voteabstainnames=$10, voteabsent=$11, voteabsentnames=$12 WHERE ID=$13`,
		b.Title, b.Session, b.Pass, b.VoteDate, b.VoteYay, b.VoteYayNames, b.VoteNay,
		b.VoteNayNames, b.VoteAbstain, b.VoteAbstainNames, b.VoteAbsent,
		b.VoteAbsentNames, b.ID)

	return err
}

func (b *bill) deleteBill(db *sql.DB) error {
	_, err := db.Exec("DELETE FROM bills WHERE id=$1", b.ID)

	return err
}

func (b *bill) createBill(db *sql.DB) error {
	err := db.QueryRow(
		`INSERT INTO legislators(title, session, pass, votedate, voteyay, `+
			`voteyaynames, votenay, votenaynames, voteabstain, voteabstainnames,`+
			`voteabsent, voteabsentnames VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10,`+
			` $11, $12) RETURNING id`, b.Title, b.Session, b.Pass, b.VoteDate, b.VoteYay,
		b.VoteYayNames, b.VoteNay, b.VoteNayNames, b.VoteAbstain, b.VoteAbstainNames,
		b.VoteAbsent, b.VoteAbsentNames).Scan(&b.ID)

	if err != nil {
		return err
	}

	return nil
}

/******************************************************************************/
// Get Slice of bills and Legislators
// Get Slice of all Legislators
func getAllLegislators(db *sql.DB) ([]legislator, error) {
	rows, err := db.Query("SELECT * FROM legislators")

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	legislators := []legislator{}

	for rows.Next() {
		var l legislator
		err := rows.Scan(&l.ID, &l.LastName, &l.FirstName, &l.StartYear, &l.EndYear,
			&l.Standing, &l.Party)
		if err != nil {
			return nil, err
		}
		legislators = append(legislators, l)
	}

	return legislators, nil
}

// Get Slice of All Bills
func getAllBills(db *sql.DB) ([]bill, error) {
	rows, err := db.Query("SELECT * FROM bills")

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	bills := []bill{}

	for rows.Next() {
		var b bill
		err := rows.Scan(&b.ID, &b.Title, &b.Session, &b.Pass, &b.VoteDate,
			&b.VoteYay, &b.VoteYayNames, &b.VoteNay, &b.VoteNayNames, &b.VoteAbstain,
			&b.VoteAbstainNames, &b.VoteAbsent, &b.VoteAbsentNames)
		if err != nil {
			return nil, err
		}
		bills = append(bills, b)
	}

	return bills, nil
}

/******************************************************************************/
